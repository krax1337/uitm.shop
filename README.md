# UITM.Shop - The best book shop in the UITM

## How to access?

Simply go to https://uitm-shop.pl-realestate.dev

## How to run it?

Simply use the docker

```bash
docker run -it --rm --name uitm-shop -p 8080:8080 $(docker build -q .)
```

Then just open it in browser: http://localhost:8080