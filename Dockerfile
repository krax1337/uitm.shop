﻿FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build

WORKDIR /build

COPY . .

RUN dotnet restore -f UITM.Shop.sln -r linux-x64 && \
    dotnet build UITM.Shop.sln -c Release && \
    dotnet test UITM.Shop.sln -c Release && \
    dotnet publish UITM.Shop.sln -r linux-x64 -c Release -p:PublishDir=./publish

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS runtime

WORKDIR /app

COPY --from=build /build/UITM.Shop/publish .

EXPOSE 8080

ENV ASPNETCORE_URLS=http://+:8080

ENTRYPOINT ["dotnet", "UITM.Shop.dll"]