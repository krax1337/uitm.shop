using System.Data.SQLite;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddRazorPages();

builder.Services.AddDistributedMemoryCache();

builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromSeconds(36000);
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment()) app.UseExceptionHandler("/Error");

app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.UseSession();

app.MapRazorPages();

var connectionString = "Data Source=db.sqlite";
using (var connection = new SQLiteConnection(connectionString))
{
    connection.Open();

    var sql = @"
        CREATE TABLE IF NOT EXISTS messages (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            firstname TEXT,
            lastname TEXT,
            email TEXT,
            phone TEXT,
            subject TEXT,
            message TEXT,
            created_at DATETIME  NOT NULL DEFAULT CURRENT_TIMESTAMP
        );

        CREATE TABLE IF NOT EXISTS books (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            title TEXT,
            authors TEXT,
            isbn TEXT UNIQUE,
            num_pages INT,
            price DECIMAL(16, 2),
            category TEXT,
            description TEXT,
            image_filename TEXT,
            created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
        );

        CREATE TABLE IF NOT EXISTS users (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            firstname TEXT,
            lastname TEXT,
            email TEXT UNIQUE,
	        phone TEXT,
	        address TEXT,
	        password TEXT,
	        role TEXT CHECK (role IN('admin', 'client')),
            created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
        );

        CREATE TABLE IF NOT EXISTS password_resets (
            email TEXT NOT NULL PRIMARY KEY,
	        token TEXT NOT NULL,
            created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
        );

        INSERT OR IGNORE  INTO users (firstname, lastname, email, phone, address, password, role)
        VALUES ('Admin', 'Admin', 'admin@example.com', '12345678', 'NY', 'AQAAAAIAAYagAAAAEN96yLH78q98a2gbQCuGbvq1D+LoqHMzK0eQEWrgfZoBoCHiyrnyZZhf09i8mQwq2A==', 'admin');
    
            CREATE TABLE  IF NOT EXISTS orders (
                        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT ,
                        client_id INT,
                        order_date DATETIME,
                        shipping_fee DECIMAL (16, 2),
                        delivery_address VARCHAR (255),
                        payment_method VARCHAR (50),
                        payment_status VARCHAR(20) CHECK (payment_status IN('pending', 'accepted', 'canceled')),
                        order_status VARCHAR(20) CHECK (
                                order_status IN('created', 'accepted', 'canceled', 'shipped', 'delivered', 'returned')
                            )
);

CREATE TABLE IF NOT EXISTS order_items (
                             id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                             order_id INTEGER,
                             book_id INTEGER,
                             quantity INTEGER,
                             unit_price DECIMAL (16, 2) NOT NULL
);

INSERT OR IGNORE INTO books (title, authors, isbn, num_pages, price, category, description, image_filename)
VALUES
    ('Code: The Hidden Language of Computer Hardware and Software', 'by Charles Petzold (Author)', '978-0137909100', 480, 34.07, 'Technology', 'The classic guide to how computers work, updated with new chapters and interactive graphics', 'code.jpg'),
    ('Python Crash Course, 3rd Edition: A Hands-On, Project-Based Introduction to Programming', 'by Eric Matthes (Author)', '978-1718502703', 552, 30.49, 'Technology', 'Python Crash Course is the world’s bestselling programming book, with over 1,500,000 copies sold to date!', 'python.jpg'),
    ('The Rust Programming Language, 2nd Edition', 'by Steve Klabnik (Author), Carol Nichols (Author)', '978-1718503106', 560, 38.99, 'Technology', 'With over 50,000 copies sold, The Rust Programming Language is the quintessential guide to programming in Rust.', 'rust.jpg'),
    ('Fundamentals of Software Architecture: An Engineering Approach', 'by Mark Richards (Author), Neal Ford (Author)', '978-1492043454', 419, 30, 'Technology', 'This book provides the first comprehensive overview of software architecture’s many aspects. Aspiring and existing architects alike will examine architectural characteristics, architectural patterns, component determination, diagramming and presenting architecture, evolutionary architecture, and many other topics.', 'software.jpg'),
    ('Software Engineering at Google: Lessons Learned from Programming Over Time', 'by Titus Winters (Author), Tom Manshreck (Author), Hyrum Wright (Author)', '978-1492082798', 599, 32.99, 'Technology', 'This book emphasizes this difference between programming and software engineering.', 'google.jpg'),
    ('C Programming Language, 2nd Edition', 'by Brian W. Kernighan (Author), Dennis M. Ritchie (Author)', '978-0131103627', 272, 45.46, 'Technology', 'The authors present the complete guide to ANSI standard C language programming.', 'c.jpg');
            ";

    using (var command = new SQLiteCommand(sql, connection))
    {
        command.ExecuteNonQuery();
    }
}


app.Run();