using System.ComponentModel.DataAnnotations;
using System.Data.SQLite;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace UITM.Shop.Pages.Auth;

// [RequireNoAuth]
[BindProperties]
public class RegisterModel : PageModel
{
    public string errorMessage = "";
    public string successMessage = "";

    [Required(ErrorMessage = "The First Name is required")]
    public string Firstname { get; set; } = "";

    [Required(ErrorMessage = "The Last Name is required")]
    public string Lastname { get; set; } = "";

    [Required(ErrorMessage = "The Email is required")]
    [EmailAddress]
    public string Email { get; set; } = "";

    public string? Phone { get; set; } = "";

    [Required(ErrorMessage = "The Address is required")]
    public string Address { get; set; } = "";

    [Required(ErrorMessage = "Password is required")]
    [StringLength(50, ErrorMessage = "Password must be between 5 and 50 characters", MinimumLength = 5)]
    public string Password { get; set; } = "";

    [Required(ErrorMessage = "Confirm Password is required")]
    [Compare("Password", ErrorMessage = "Password and Confirm Password do not match")]
    public string ConfirmPassword { get; set; } = "";


    public void OnGet()
    {
    }

    public void OnPost()
    {
        if (!ModelState.IsValid)
        {
            errorMessage = "Data validation failed";
            return;
        }

        // successfull data validation
        if (Phone == null) Phone = "";

        // add the user details to the database
        var connectionString = "Data Source=db.sqlite";

        try
        {
            using (var connection = new SQLiteConnection(connectionString))
            {
                connection.Open();
                var sql = "INSERT INTO users " +
                          "(firstname, lastname, email, phone, address, password, role) VALUES " +
                          "(@firstname, @lastname, @email, @phone, @address, @password, 'client');";

                var passwordHasher = new PasswordHasher<IdentityUser>();
                var hashedPassword = passwordHasher.HashPassword(new IdentityUser(), Password);

                using (var command = new SQLiteCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@firstname", Firstname);
                    command.Parameters.AddWithValue("@lastname", Lastname);
                    command.Parameters.AddWithValue("@email", Email);
                    command.Parameters.AddWithValue("@phone", Phone);
                    command.Parameters.AddWithValue("@address", Address);
                    command.Parameters.AddWithValue("@password", hashedPassword);

                    command.ExecuteNonQuery();
                }
            }
        }
        catch (Exception ex)
        {
            if (ex.Message.Contains(Email))
                errorMessage = "Email address already used";
            else
                errorMessage = ex.Message;

            return;
        }

        // initialize the authenticated session => add the user details to the session data
        try
        {
            using (var connection = new SQLiteConnection(connectionString))
            {
                connection.Open();
                var sql = "SELECT * FROM users WHERE email=@email";

                using (var command = new SQLiteCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@email", Email);

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var id = reader.GetInt32(0);
                            var firstname = reader.GetString(1);
                            var lastname = reader.GetString(2);
                            var email = reader.GetString(3);
                            var phone = reader.GetString(4);
                            var address = reader.GetString(5);
                            //string hashedPassword = reader.GetString(6);
                            var role = reader.GetString(7);
                            var created_at = reader.GetDateTime(8).ToString("MM/dd/yyyy");

                            HttpContext.Session.SetInt32("id", id);
                            HttpContext.Session.SetString("firstname", firstname);
                            HttpContext.Session.SetString("lastname", lastname);
                            HttpContext.Session.SetString("email", email);
                            HttpContext.Session.SetString("phone", phone);
                            HttpContext.Session.SetString("address", address);
                            HttpContext.Session.SetString("role", role);
                            HttpContext.Session.SetString("created_at", created_at);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            errorMessage = ex.Message;
            return;
        }


        successMessage = "Account created successfully";

        // redirect to the home page
        Response.Redirect("/");
    }
}