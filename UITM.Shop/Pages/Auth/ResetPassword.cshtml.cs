using System.ComponentModel.DataAnnotations;
using System.Data.SQLite;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using UITM.Shop.MyHelpers;

namespace UITM.Shop.Pages.Auth;

[RequireNoAuth]
public class ResetPasswordModel : PageModel
{
    public string errorMessage = "";
    public string successMessage = "";

    [BindProperty]
    [Required(ErrorMessage = "Password is required")]
    [StringLength(50, ErrorMessage = "Password must be between 5 and 50 characters", MinimumLength = 5)]
    public string Password { get; set; } = "";

    [BindProperty]
    [Required(ErrorMessage = "Confirm Password is required")]
    [Compare("Password", ErrorMessage = "Password and Confirm Password do not match")]
    public string ConfirmPassword { get; set; } = "";

    public void OnGet()
    {
        string token = Request.Query["token"];
        if (string.IsNullOrEmpty(token)) Response.Redirect("/");
    }

    public void OnPost()
    {
        string token = Request.Query["token"];
        if (string.IsNullOrEmpty(token))
        {
            Response.Redirect("/");
            return;
        }

        if (!ModelState.IsValid)
        {
            errorMessage = "Data validation failed";
            return;
        }


        // connect to database and update the password
        try
        {
            var connectionString = "Data Source=db.sqlite";

            using (var connection = new SQLiteConnection(connectionString))
            {
                connection.Open();

                // 1) check if the token is valid, and get the user email address from password_resets
                var email = "";
                var sql = "SELECT * FROM password_resets WHERE token=@token";
                using (var command = new SQLiteCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@token", token);
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            email = reader.GetString(0);
                        }
                        else
                        {
                            errorMessage = "Wrong or Expired Token";
                            return;
                        }
                    }
                }

                // 2) encrypt the new password and update the user password in users table
                var passwordHasher = new PasswordHasher<IdentityUser>();
                var hashedPassword = passwordHasher.HashPassword(new IdentityUser(), Password);

                sql = "UPDATE users SET password=@password WHERE email=@email";
                using (var command = new SQLiteCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@password", hashedPassword);
                    command.Parameters.AddWithValue("@email", email);

                    command.ExecuteNonQuery();
                }

                // 3) delete the token from the database (password_resets table)
                sql = "DELETE FROM password_resets WHERE email=@email";
                using (var command = new SQLiteCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@email", email);

                    command.ExecuteNonQuery();
                }
            }
        }
        catch (Exception ex)
        {
            errorMessage = ex.Message;
            return;
        }

        successMessage = "Password reset successfully";
    }
}