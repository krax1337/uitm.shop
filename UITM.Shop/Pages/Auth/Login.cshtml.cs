using System.ComponentModel.DataAnnotations;
using System.Data.SQLite;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace UITM.Shop.Pages.Auth;

[BindProperties]
public class LoginModel : PageModel
{
    public string errorMessage = "";
    public string successMessage = "";

    [Required(ErrorMessage = "The Email is required")]
    [EmailAddress]
    public string Email { get; set; } = "";

    [Required(ErrorMessage = "Password is required")]
    public string Password { get; set; } = "";


    public void OnGet()
    {
    }

    public void OnPost()
    {
        if (!ModelState.IsValid)
        {
            errorMessage = "Data validation failed";
            return;
        }

        // successfull data validation

        // connect to database and check the user credentials
        try
        {
            var connectionString = "Data Source=db.sqlite";
            using (var connection = new SQLiteConnection(connectionString))
            {
                connection.Open();
                var sql = "SELECT * FROM users WHERE email=@email";

                using (var command = new SQLiteCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@email", Email);
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var id = reader.GetInt32(0);
                            var firstname = reader.GetString(1);
                            var lastname = reader.GetString(2);
                            var email = reader.GetString(3);
                            var phone = reader.GetString(4);
                            var address = reader.GetString(5);
                            var hashedPassword = reader.GetString(6);
                            var role = reader.GetString(7);

                            Console.WriteLine(id);

                            // verify the password
                            var passwordHasher = new PasswordHasher<IdentityUser>();
                            var result = passwordHasher.VerifyHashedPassword(new IdentityUser(),
                                hashedPassword, Password);

                            if (result == PasswordVerificationResult.Success
                                || result == PasswordVerificationResult.SuccessRehashNeeded)
                            {
                                // successful password verification => initialize the session
                                HttpContext.Session.SetInt32("id", id);
                                HttpContext.Session.SetString("firstname", firstname);
                                HttpContext.Session.SetString("lastname", lastname);
                                HttpContext.Session.SetString("email", email);
                                HttpContext.Session.SetString("phone", phone);
                                HttpContext.Session.SetString("address", address);
                                HttpContext.Session.SetString("role", role);

                                // the user is authenticated successfully => redirect to the home page
                                Response.Redirect("/");
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            errorMessage = ex.Message;
            return;
        }

        // Wrong Email or Password
        errorMessage = "Wrong Email or Password";
    }
}