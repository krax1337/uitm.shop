using System.ComponentModel.DataAnnotations;
using System.Data.SQLite;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace UITM.Shop.Pages.Auth;

public class ForgotPasswordModel : PageModel
{
    public string errorMessage = "";
    public string successMessage = "";

    [BindProperty]
    [Required(ErrorMessage = "The Email is required")]
    [EmailAddress]
    public string Email { get; set; } = "";

    public void OnGet()
    {
    }

    public void OnPost()
    {
        if (!ModelState.IsValid)
        {
            errorMessage = "Data validation failed";
            return;
        }

        // try
        // {
        var connectionString = "Data Source=db.sqlite";

        using (var connection = new SQLiteConnection(connectionString))
        {
            connection.Open();
            var sql = "SELECT * FROM users WHERE email=@email";

            using (var command = new SQLiteCommand(sql, connection))
            {
                command.Parameters.AddWithValue("@email", Email);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var firstname = reader.GetString(1);
                        var lastname = reader.GetString(2);

                        var token = "token";
                        reader.Close();
                        connection.Close();
                        Console.WriteLine("CLOSED CONNECTION INITIAL");
                        SaveToken(Email, token);
                        Console.WriteLine("TOKEN WRITED");
                    }
                    else
                    {
                        errorMessage = "We have no user with this email address";
                        return;
                    }
                }
            }

            connection.Close();
        }
        // }
        // catch (Exception ex)
        // {
        //     errorMessage = ex.Message;
        //     return;
        // }

        successMessage = "Please check your email and click on the reset password link";
    }

    private void SaveToken(string email, string token)
    {
        // try
        // {
        var connectionString = "Data Source=db.sqlite";

        using (var connection = new SQLiteConnection(connectionString))
        {
            connection.Open();

            var sql = "DELETE FROM password_resets WHERE email=@email";

            using (var command = new SQLiteCommand(sql, connection))
            {
                command.Parameters.AddWithValue("@email", email);

                command.ExecuteNonQuery();
            }

            sql = "INSERT INTO password_resets (email, token) VALUES (@email, @token)";

            using (var command = new SQLiteCommand(sql, connection))
            {
                command.Parameters.AddWithValue("@email", email);
                command.Parameters.AddWithValue("@token", token);

                command.ExecuteNonQuery();
            }

            connection.Close();
        }
        // }
        // catch (Exception ex)
        // {
        //     Console.WriteLine(ex.Message);
        // }
    }
}