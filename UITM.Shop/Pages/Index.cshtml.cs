﻿using UITM.Pages.Admin.Books;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data.SQLite;

namespace UITM.Shop.Pages
{
    public class IndexModel : PageModel
    {
        public List<BookInfo> listNewestBooks = new List<BookInfo>();
        public List<BookInfo> listTopSales = new List<BookInfo>();

        public void OnGet()
        {
            try
            {
                string connectionString = "Data Source=db.sqlite";

                using (SQLiteConnection connection = new SQLiteConnection(connectionString))
                {
                    connection.Open();

                    string SQLite = "SELECT * FROM books ORDER BY created_at DESC LIMIT 4";
                    using (SQLiteCommand command = new SQLiteCommand(SQLite, connection))
                    {
                        using (SQLiteDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                BookInfo bookInfo = new BookInfo();
                                bookInfo.Id = reader.GetInt32(0);
                                bookInfo.Title = reader.GetString(1);
                                bookInfo.Authors = reader.GetString(2);
                                bookInfo.Isbn = reader.GetString(3);
                                bookInfo.NumPages = reader.GetInt32(4);
                                bookInfo.Price = reader.GetDecimal(5);
                                bookInfo.Category = reader.GetString(6);
                                bookInfo.Description = reader.GetString(7);
                                bookInfo.ImageFileName = reader.GetString(8);
                                bookInfo.CreatedAt = reader.GetDateTime(9).ToString("MM/dd/yyyy");

                                listNewestBooks.Add(bookInfo);
                            }
                        }
                    }

                    SQLite = "SELECT books.*, (" +
                       "SELECT SUM(order_items.quantity) FROM order_items WHERE books.id = order_items.book_id" +
                       ") AS total_sales " +
                       "FROM books " +
                       "ORDER BY total_sales DESC LIMIT 4;";

                    using (SQLiteCommand command = new SQLiteCommand(SQLite, connection))
                    {
                        using (SQLiteDataReader reader = command.ExecuteReader())
                        {
                            while(reader.Read())
                            {
                                BookInfo bookInfo = new BookInfo();
                                bookInfo.Id = reader.GetInt32(0);
                                bookInfo.Title = reader.GetString(1);
                                bookInfo.Authors = reader.GetString(2);
                                bookInfo.Isbn = reader.GetString(3);
                                bookInfo.NumPages = reader.GetInt32(4);
                                bookInfo.Price = reader.GetDecimal(5);
                                bookInfo.Category = reader.GetString(6);
                                bookInfo.Description = reader.GetString(7);
                                bookInfo.ImageFileName = reader.GetString(8);
                                bookInfo.CreatedAt = reader.GetDateTime(9).ToString("MM/dd/yyyy");

                                listTopSales.Add(bookInfo);
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}