using System.Data.SQLite;
using Microsoft.AspNetCore.Mvc.RazorPages;
using UITM.Pages.Admin.Books;
using UITM.Shop.MyHelpers;

namespace UITM.Shop.Pages.Admin.Orders;

[RequireAuth(RequiredRole = "admin")]
public class IndexModel : PageModel
{
    private readonly int pageSize = 3; // orders per page
    public List<OrderInfo> listOrders = new();

    public int page = 1; // the current html page
    public int totalPages;

    public void OnGet()
    {
        try
        {
            string requestPage = Request.Query["page"];
            page = int.Parse(requestPage);
        }
        catch (Exception ex)
        {
            page = 1;
        }

        try
        {
            var connectionString = "Data Source=db.sqlite";
            using (var connection = new SQLiteConnection(connectionString))
            {
                connection.Open();

                var SQLiteCount = "SELECT COUNT(*) FROM orders";
                using (var command = new SQLiteCommand(SQLiteCount, connection))
                {
                    decimal count = (long)command.ExecuteScalar();
                    totalPages = (int)Math.Ceiling(count / pageSize);
                }

                var SQLite = "SELECT * FROM orders ORDER BY id DESC";
                SQLite += " LIMIT @pageSize OFFSET @skip";
                using (var command = new SQLiteCommand(SQLite, connection))
                {
                    command.Parameters.AddWithValue("@skip", (page - 1) * pageSize);
                    command.Parameters.AddWithValue("@pageSize", pageSize);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var orderInfo = new OrderInfo();
                            orderInfo.id = reader.GetInt32(0);
                            orderInfo.clientId = reader.GetInt32(1);
                            orderInfo.orderDate = reader.GetDateTime(2).ToString("MM/dd/yyyy");
                            orderInfo.shippingFee = reader.GetDecimal(3);
                            orderInfo.deliveryAddress = reader.GetString(4);
                            orderInfo.paymentMethod = reader.GetString(5);
                            orderInfo.paymentStatus = reader.GetString(6);
                            orderInfo.orderStatus = reader.GetString(7);

                            orderInfo.items = OrderInfo.getOrderItems(orderInfo.id);

                            listOrders.Add(orderInfo);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
}

public class OrderItemInfo
{
    public int bookId;

    public BookInfo bookInfo = new BookInfo();
    public int id;
    public int orderId;
    public int quantity;
    public decimal unitPrice;
}

public class OrderInfo
{
    public int clientId;
    public string deliveryAddress;
    public int id;

    public List<OrderItemInfo> items = new();
    public string orderDate;
    public string orderStatus;
    public string paymentMethod;
    public string paymentStatus;
    public decimal shippingFee;

    public static List<OrderItemInfo> getOrderItems(int orderId)
    {
        var items = new List<OrderItemInfo>();

        try
        {
            var connectionString = "Data Source=db.sqlite";
            using (var connection = new SQLiteConnection(connectionString))
            {
                connection.Open();
                var SQLite = "SELECT order_items.*, books.* FROM order_items, books " +
                          "WHERE order_items.order_id=@order_id AND order_items.book_id = books.id";

                using (var command = new SQLiteCommand(SQLite, connection))
                {
                    command.Parameters.AddWithValue("@order_id", orderId);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var item = new OrderItemInfo();

                            item.id = reader.GetInt32(0);
                            item.orderId = reader.GetInt32(1);
                            item.bookId = reader.GetInt32(2);
                            item.quantity = reader.GetInt32(3);
                            item.unitPrice = reader.GetDecimal(4);

                            item.bookInfo.Id = reader.GetInt32(5);
                            item.bookInfo.Title = reader.GetString(6);
                            item.bookInfo.Authors = reader.GetString(7);
                            item.bookInfo.Isbn = reader.GetString(8);
                            item.bookInfo.NumPages = reader.GetInt32(9);
                            item.bookInfo.Price = reader.GetDecimal(10);
                            item.bookInfo.Category = reader.GetString(11);
                            item.bookInfo.Description = reader.GetString(12);
                            item.bookInfo.ImageFileName = reader.GetString(13);
                            item.bookInfo.CreatedAt = reader.GetDateTime(14).ToString("MM/dd/yyyy");

                            items.Add(item);
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }

        return items;
    }
}