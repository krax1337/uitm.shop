using System.Data.SQLite;
using Microsoft.AspNetCore.Mvc.RazorPages;
using UITM.Shop.MyHelpers;

namespace UITM.Shop.Pages.Admin.Messages;

[RequireAuth(RequiredRole = "admin")]
public class DetailsModel : PageModel
{
    private readonly IConfiguration Configuration;
    public MessageInfo messageInfo = new();

    public DetailsModel(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public void OnGet()
    {
        string requestId = Request.Query["id"];

        try
        {
            var connectionString = "Data Source=db.sqlite";
            using (var connection = new SQLiteConnection(connectionString))
            {
                connection.Open();
                var sql = "SELECT * FROM messages WHERE id=@id";

                using (var command = new SQLiteCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@id", requestId);

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            messageInfo.Id = reader.GetInt32(0);
                            messageInfo.FirstName = reader.GetString(1);
                            messageInfo.LastName = reader.GetString(2);
                            messageInfo.Email = reader.GetString(3);
                            messageInfo.Phone = reader.GetString(4);
                            messageInfo.Subject = reader.GetString(5);
                            messageInfo.Message = reader.GetString(6);
                            messageInfo.CreatedAt = reader.GetDateTime(7).ToString("MM/dd/yyyy");
                        }
                        else
                        {
                            Response.Redirect("/Admin/Messages/Index");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            Response.Redirect("/Admin/Messages/Index");
        }
    }
}