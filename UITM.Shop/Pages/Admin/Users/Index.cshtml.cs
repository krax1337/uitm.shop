using System.Data.SQLite;
using Microsoft.AspNetCore.Mvc.RazorPages;
using UITM.Shop.MyHelpers;

namespace UITM.Shop.Pages.Admin.Users;

[RequireAuth(RequiredRole = "admin")]
public class IndexModel : PageModel
{
    private readonly int pageSize = 5; // users per page
    public List<UserInfo> listUsers = new();

    public int page = 1; // the current html page
    public int totalPages;

    public void OnGet()
    {
        page = 1;
        string requestPage = Request.Query["page"];
        if (requestPage != null)
            try
            {
                page = int.Parse(requestPage);
            }
            catch (Exception ex)
            {
                page = 1;
            }

        try
        {
            var connectionString = "Data Source=db.sqlite";
            using (var connection = new SQLiteConnection(connectionString))
            {
                connection.Open();

                // find the number of users
                var sqlCount = "SELECT COUNT(*) FROM users";
                using (var command = new SQLiteCommand(sqlCount, connection))
                {
                    decimal count = (long)command.ExecuteScalar();
                    totalPages = (int)Math.Ceiling(count / pageSize);
                }

                var sql = "SELECT * FROM users ORDER BY id DESC";
                sql += " LIMIT @pageSize OFFSET @skip";
                using (var command = new SQLiteCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@skip", (page - 1) * pageSize);
                    command.Parameters.AddWithValue("@pageSize", pageSize);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var userInfo = new UserInfo();

                            userInfo.id = reader.GetInt32(0);
                            userInfo.firstname = reader.GetString(1);
                            userInfo.lastname = reader.GetString(2);
                            userInfo.email = reader.GetString(3);
                            userInfo.phone = reader.GetString(4);
                            userInfo.address = reader.GetString(5);
                            userInfo.password = reader.GetString(6);
                            userInfo.role = reader.GetString(7);
                            userInfo.createdAt = reader.GetDateTime(8).ToString("MM/dd/yyyy");

                            listUsers.Add(userInfo);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
}

public class UserInfo
{
    public string address;
    public string createdAt;
    public string email;
    public string firstname;
    public int id;
    public string lastname;
    public string password;
    public string phone;
    public string role;
}