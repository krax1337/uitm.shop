using UITM.Pages.Admin.Books;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data.SQLite;

namespace UITM.Shop.Pages
{
    [BindProperties(SupportsGet = true)]
    public class BooksModel : PageModel
    {
        public string? Search { get; set; }
        public string PriceRange { get; set; } = "any";
        public string PageRange { get; set; } = "any";
        public string Category { get; set; } = "any";

        public List<BookInfo> listBooks = new List<BookInfo>();


        public int page = 1; // the current html page
        public int totalPages = 0;
        private readonly int pageSize = 5; // books per page

        public void OnGet()
        {
            page = 1;
            string requestPage = Request.Query["page"];
            if (requestPage != null)
            {
                try
                {
                    page = int.Parse(requestPage);
                }
                catch (Exception ex)
                {
                    page = 1;
                }
            }

            try
            {
                string connectionString = "Data Source=db.sqlite";

                using (SQLiteConnection connection = new SQLiteConnection(connectionString))
                {
                    connection.Open();

                    string SQLiteCount = "SELECT COUNT(*) FROM books";
                    SQLiteCount += " WHERE (title LIKE @search OR authors LIKE @search)";

                    if (PriceRange.Equals("0_50"))
                    {
                        SQLiteCount += " AND price <= 50";
                    }
                    else if (PriceRange.Equals("50_100"))
                    {
                        SQLiteCount += " AND price >= 50 AND price <= 100";
                    }
                    else if (PriceRange.Equals("above100"))
                    {
                        SQLiteCount += " AND price >= 100";
                    }


                    if (PageRange.Equals("0_100"))
                    {
                        SQLiteCount += " AND num_pages <= 100";
                    }
                    else if (PageRange.Equals("100_299"))
                    {
                        SQLiteCount += " AND num_pages >= 100 AND num_pages <= 299";
                    }
                    else if (PageRange.Equals("above300"))
                    {
                        SQLiteCount += " AND num_pages >= 300";
                    }


                    if (!Category.Equals("any"))
                    {
                        SQLiteCount += " AND category=@category";
                    }

                    using (SQLiteCommand command = new SQLiteCommand(SQLiteCount, connection))
                    {
                        command.Parameters.AddWithValue("@search", "%" + Search + "%");
                        command.Parameters.AddWithValue("@category", Category);

                        decimal count = (long)command.ExecuteScalar();
                        totalPages = (int)Math.Ceiling(count / pageSize);
                    }



                    string SQLite = "SELECT * FROM books";
                    SQLite += " WHERE (title LIKE @search OR authors LIKE @search)";

                    if (PriceRange.Equals("0_50"))
                    {
                        SQLite += " AND price <= 50";
                    }
                    else if (PriceRange.Equals("50_100"))
                    {
                        SQLite += " AND price >= 50 AND price <= 100";
                    }
                    else if (PriceRange.Equals("above100"))
                    {
                        SQLite += " AND price >= 100";
                    }


                    if (PageRange.Equals("0_100"))
                    {
                        SQLite += " AND num_pages <= 100";
                    }
                    else if (PageRange.Equals("100_299"))
                    {
                        SQLite += " AND num_pages >= 100 AND num_pages <= 299";
                    }
                    else if (PageRange.Equals("above300"))
                    {
                        SQLite += " AND num_pages >= 300";
                    }


                    if (!Category.Equals("any"))
                    {
                        SQLite += " AND category=@category";
                    }

                    SQLite += " ORDER BY id DESC";
                    SQLite += " LIMIT @pageSize OFFSET @skip";


                    using (SQLiteCommand command = new SQLiteCommand(SQLite, connection))
                    {
                        command.Parameters.AddWithValue("@search", "%" + Search + "%");
                        command.Parameters.AddWithValue("@category", Category);
                        command.Parameters.AddWithValue("@skip", (page - 1) * pageSize);
                        command.Parameters.AddWithValue("@pageSize", pageSize);

                        using (SQLiteDataReader reader = command.ExecuteReader())
                        {
                            while(reader.Read())
                            {
                                BookInfo bookInfo = new BookInfo();

                                bookInfo.Id = reader.GetInt32(0);
                                bookInfo.Title = reader.GetString(1);
                                bookInfo.Authors = reader.GetString(2);
                                bookInfo.Isbn = reader.GetString(3);
                                bookInfo.NumPages = reader.GetInt32(4);
                                bookInfo.Price = reader.GetDecimal(5);
                                bookInfo.Category = reader.GetString(6);
                                bookInfo.Description = reader.GetString(7);
                                bookInfo.ImageFileName = reader.GetString(8);
                                bookInfo.CreatedAt = reader.GetDateTime(9).ToString("MM/dd/yyyy");

                                listBooks.Add(bookInfo);
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
