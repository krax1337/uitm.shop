using System.Data.SQLite;
using Microsoft.AspNetCore.Mvc.RazorPages;
using UITM.Shop.MyHelpers;
using UITM.Shop.Pages.Admin.Orders;

namespace UITM.Shop.Pages.Client.Orders;

[RequireAuth(RequiredRole = "client")]
public class DetailsModel : PageModel
{
    public OrderInfo orderInfo = new();

    public void OnGet(int id)
    {
        var clientId = HttpContext.Session.GetInt32("id") ?? 0;

        if (id < 1)
        {
            Response.Redirect("/Client/Orders/Index");
            return;
        }


        try
        {
            var connectionString = "Data Source=db.sqlite";
            using (var connection = new SQLiteConnection(connectionString))
            {
                connection.Open();


                var SQLite = "SELECT * FROM orders WHERE id=@id AND client_id=@client_id";
                using (var command = new SQLiteCommand(SQLite, connection))
                {
                    command.Parameters.AddWithValue("@id", id);
                    command.Parameters.AddWithValue("@client_id", clientId);

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            orderInfo.id = reader.GetInt32(0);
                            orderInfo.clientId = reader.GetInt32(1);
                            orderInfo.orderDate = reader.GetDateTime(2).ToString("MM/dd/yyyy");
                            orderInfo.shippingFee = reader.GetDecimal(3);
                            orderInfo.deliveryAddress = reader.GetString(4);
                            orderInfo.paymentMethod = reader.GetString(5);
                            orderInfo.paymentStatus = reader.GetString(6);
                            orderInfo.orderStatus = reader.GetString(7);

                            orderInfo.items = OrderInfo.getOrderItems(orderInfo.id);
                        }
                        else
                        {
                            Response.Redirect("/Client/Orders/Index");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            Response.Redirect("/Client/Orders/Index");
        }
    }
}