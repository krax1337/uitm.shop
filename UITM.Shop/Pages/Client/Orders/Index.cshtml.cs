using System.Data.SQLite;
using Microsoft.AspNetCore.Mvc.RazorPages;
using UITM.Shop.MyHelpers;
using UITM.Shop.Pages.Admin.Orders;

namespace UITM.Shop.Pages.Client.Orders;

[RequireAuth(RequiredRole = "client")]
public class IndexModel : PageModel
{
    private readonly int pageSize = 3; // orders per page
    public List<OrderInfo> listOrders = new();

    public int page = 1; // the current html page
    public int totalPages;

    public void OnGet()
    {
        var clientId = HttpContext.Session.GetInt32("id") ?? 0;

        try
        {
            string requestPage = Request.Query["page"];
            page = int.Parse(requestPage);
        }
        catch (Exception ex)
        {
            page = 1;
        }

        try
        {
            var connectionString = "Data Source=db.sqlite";
            using (var connection = new SQLiteConnection(connectionString))
            {
                connection.Open();

                var SQLiteCount = "SELECT COUNT(*) FROM orders WHERE client_id=@client_id";
                using (var command = new SQLiteCommand(SQLiteCount, connection))
                {
                    command.Parameters.AddWithValue("@client_id", clientId);

                    decimal count = (long)command.ExecuteScalar();
                    totalPages = (int)Math.Ceiling(count / pageSize);
                }

                var SQLite = "SELECT * FROM orders WHERE client_id=@client_id ORDER BY id DESC";
                SQLite += " LIMIT @pageSize OFFSET @skip";
                using (var command = new SQLiteCommand(SQLite, connection))
                {
                    command.Parameters.AddWithValue("@client_id", clientId);
                    command.Parameters.AddWithValue("@skip", (page - 1) * pageSize);
                    command.Parameters.AddWithValue("@pageSize", pageSize);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var orderInfo = new OrderInfo();
                            orderInfo.id = reader.GetInt32(0);
                            orderInfo.clientId = reader.GetInt32(1);
                            orderInfo.orderDate = reader.GetDateTime(2).ToString("MM/dd/yyyy");
                            orderInfo.shippingFee = reader.GetDecimal(3);
                            orderInfo.deliveryAddress = reader.GetString(4);
                            orderInfo.paymentMethod = reader.GetString(5);
                            orderInfo.paymentStatus = reader.GetString(6);
                            orderInfo.orderStatus = reader.GetString(7);

                            orderInfo.items = OrderInfo.getOrderItems(orderInfo.id);

                            listOrders.Add(orderInfo);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
}